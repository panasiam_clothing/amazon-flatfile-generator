# Generate Amazon flatfiles with data from Plentymarkets


## Introduction

This application allows the user to create an amazon flatfile for all items and variations, one variation or one item from the available Plentymarkets data. The data gets pulled from the Plentymarkets REST API and will be transformed into a flatfile with a format according to the configuration and a template file.


## Installation

The application is implemented with the [poetry](https://python-poetry.org/) package manager, make sure that this dependency is properly installed.
To install poetry no matter what system you are using open the terminal and enter:

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
```

You can now install all dependencies of the application by navigating to the installation directory and running the following in the terminal:

```bash
poetry build
python -m pip install dist/amazon_flatfile_generator{current version}.tar.gz
```


## Configuration

The configuration file is located by default under `~/.config/amazon_flatfile_generator/config.ini` on a Linux system and under `C:\users\{$USER}\AppData\Local\amazon_flatfile_generator\amazon_flatfile_generator\config.ini` on a Windows system, it will be created automatically when launching the application for the first time.
You can configure the location for the configuration, data and output directories via either the command line arguments (`--config`, `--data`, `--output`) or with the environment variables (`AMAZON_FLATFILE_GENERATOR_CONFIG_DIR`, `AMAZON_FLATFILE_GENERATOR_DATA_DIR`, `AMAZON_FLATFILE_GENERATOR_OUTPUT_DIR`).

### Basic configuration

The application requires some basic information in order to work properly, you have to provide the URL for the Plentymarkets REST API endpoint (from Setup->Settings->API->Data->Host (prepended by 'https://')). The relative path to the template spreadsheet within the data folder, as well as the sheetname.

Example configuration:

```bash
[general]
template_file = template/template.xlsx
template_sheet = Template

[plenty]
base_url = https://company.plentymarkets-cloud01.com
```

### Default choice configuration

You can configure default choices for the three option menues: **language**, **file-format** and **origin**. Simply, insert the default choice as value for the configuration options [`default_language`, `default_file_format`, `default_origin`] within the `general` section.

### Flatfile configuration

The Program allows the user to filter for specific languages, in order to get a selection of specific languages a section for languages is needed in the config:

```ìni
[languages]
{language_name} = {language_value}
```

**Note:** The ```language_value``` needs to be a country abbreviation in the [ISO-3166-1 format](https://developers.plentymarkets.com/en-gb/developers/main/rest-api-guides/getting-started.html#_language_codes)

To insert the correct data into the flatfile the application needs to know what attributes/features belong to which column in the template file. For that the application needs a configuration in a specific format.

```ini
[columns]
```

Format for non selection features:

```ini
{column} = properties,propertyId:{Feature Id},feature
```

Format for selection features:

```ini
{column} = properties,propertyId:{Feature Id},selection
```

Format for attributes:

```ini
{column} = variationAttributeValues,attributeId:{Attribute Id},attributeValue,backendName
```

Format for images:

```ini
{column} = images,{position} | the positions are reordered for better usage in ascending order, the lowest position will get the position "0", the next will get position "1", etc
```

Special values:

```ini
item_sku = number
item_name = name
parent_sku = parent,itemId
external_product_id = variationBarcodes,barcodeId:{Barcode Id you want to use},code
standard_price = variationSalesPrices,salesPriceId:4,price
```

To handle columns that require exclusive values for either parents or children add the `-parent` or `-child` flag to the configuration:

```ini
{columns} = {-parent/-child},{value}
```

For columns which require a fixed value add the `-default` flag to the configuration:

```ini
{column} = -default,{value}
```

**NOTE:** To define a default value for a parent/child, the flag for the parents/child need to be used before the default flag

You can also define alternative locations of the required data in the [alternatives] section in the config, which will be used, if there is no data found in the original data location.

```ini
[alternatives]
{column_1} = {location_1}
{column_2} = {location_2}
...
```

**NOTE:** There can only be one alternative for each column in the flatfile.

## Usage

In order to use the application you can either open the CLI and run:

```bash
python -m amazon_flatfile_generator
```

The created files will be stored into the Documents directory of the current user by default.

Or you can use a desktop shortcut:

Windows:

Simply use the shortcut.py script as the desktop shortcut.

Ubuntu:

You will need to [create](https://gist.github.com/nathakits/7efb09812902b533999bda6793c5e872) a new desktop shortcut pointing to the shortcut.py file.
